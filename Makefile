PROTODIR=./$(service)-service/proto/$(service)

protoc:
	protoc --proto_path=$$GOPATH/src:. --go_out=. --micro_out=. \
		$(PROTODIR)/*.proto

build:
	docker build --build-arg SERVICE=$(service) -t shippy-service-$(service) .

run:
	docker run -p 50052:50051 \
		-e MICRO_SERVER_ADDRESS=":50051" shippy-service-vessel

clean:
	rm -fr $(PROTODIR)/*.go
